# pdtemplatenuxt2test

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
{
  "functions": {
    "source": "functions",
    "predeploy": [
      "npm --prefix src run lint && npm --prefix src run build && del functions/nuxt && copy -r src/nuxt/ functions/nuxt/ && copy src/nuxt.config.js functions/ && npm --prefix functions run lint"
    ]
  },
  "hosting": {
    "predeploy": [
      "del  public/* && mkdir -p public/_nuxt && copy -a src/nuxt/dist/client/. public/_nuxt && copy -a src/static/. public/ && copy -a public_base/. public/"
    ],
    "public": "public",
    "ignore": [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ],
    "rewrites": [
      {
        "source": "**",
        "destination": "/index.html",// remove this & ssrapp errors 404 but we get to the home page
        "function": "ssrapp"
      }
    ]
  }
}
